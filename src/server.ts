import express from 'express';
import { Http2Server } from 'http2';
import { Registry } from 'prom-client';

export class Server {
  private port: number;
  private app = express();
  private server: Http2Server | null = null;

  constructor(registry: Registry, port: number) {
    this.registerMetrics(registry);
    this.port = port;
  }

  public start() {
    this.server = this.app.listen(this.port);
  }

  public stop() {
    if(this.server != null) {
      this.server.close();
    }
  }

  private registerMetrics(registry: Registry) {
    this.app.get('/metrics', (_request, response) => {
      response.set('Content-Type', registry.contentType);
      response.end(registry.metrics());
    });
  }
}
